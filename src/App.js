import React, {useState, Fragment} from 'react';
import Formulario from "./Components/Formulario";
import Cita from "./Components/CitaList";

function App() {

  const [citas,guardarCitas] = useState([]);
  
  let crearCita = (cita) => {

      const nuevaCita =[...citas,cita]
      guardarCitas(nuevaCita)

  }

  let eliminarCita = (i) => {

      const quitarCita = [...citas]
      quitarCita.splice(i,1)
      guardarCitas(quitarCita)
  }

  return (

  <Fragment>
    <h1>Administrador de pacientes</h1>
    <div className="container">
      <div className="row">
        <div className="one-half column">
          <Formulario crearCita={crearCita}/>
        </div>
        <div className="one-half column">
        <h2>Citas</h2>
            {citas.map((citas, index) => (
                <Cita key={index} index={index} cita={citas} eliminarCita={eliminarCita}/>
            ))}
        </div>
      </div>
    </div>
  </Fragment>
  );
}

export default App;
