import React, { useState,Fragment } from 'react';

function Formulario({crearCita}) {

    const [nombreMascota, setNombre] = useState('');
    const [propietario, setPropietario] = useState('');
    const [fecha, setFecha] = useState('');
    const [hora, setHora] = useState('');
    const [sintoma, setSintoma] = useState('');

    function handleOnSubmit(e) {
        e.preventDefault();

        crearCita({
            nombreMascota,
            propietario,
            fecha,
            hora,
            sintoma
        });

        e.currentTarget.reset();
    }

    return (
        <Fragment>
            <h2>Crear Cita</h2>

            <form onSubmit={handleOnSubmit}>
                <label>Nombre Mascota</label>
                <input
                    type="text"
                    name="mascota"
                    className="u-full-width"
                    placeholder="Nombre Mascota"
                    onChange={e => setNombre(e.target.value)}
                    value={nombreMascota}
                />

                <label>Nombre Dueño</label>
                <input
                    type="text"
                    name="propietario"
                    className="u-full-width"
                    placeholder="Nombre Dueño de la Mascota"
                    onChange={e => setPropietario(e.target.value)}
                    value={propietario}
                />

                <label>Fecha</label>
                <input
                    type="date"
                    className="u-full-width"
                    name="fecha"
                    onChange={e => setFecha(e.target.value)}
                    value={fecha}
                />

                <label>Hora</label>
                <input
                    type="time"
                    className="u-full-width"
                    name="hora"
                    onChange={e => setHora(e.target.value)}
                    value={hora}
                />

                <label>Sintomas</label>
                <textarea
                    className="u-full-width"
                    name="sintomas"
                    onChange={e => setSintoma(e.target.value)}
                    value={sintoma}
                />

                <button type="submit" className="button-primary u-full-width">Agregar</button>
            </form>
        </Fragment>
    )
}

export default Formulario
